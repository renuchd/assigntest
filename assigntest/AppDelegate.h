//
//  AppDelegate.h
//  assigntest
//
//  Created by Click labs 114 on 10/8/15.
//  Copyright (c) 2015 Hitesh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

