//
//  ViewController.m
//  assigntest
//
//  Created by Click labs 114 on 10/8/15.
//  Copyright (c) 2015 Hitesh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self firstname];
    [self lastname];
    [self email];
    [self phone];
    [self lable];
    [self labelandbutton];
    [self buttons];
    [self image];
    [self infolable];
    [self lineView];

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"image.png"]]];
}

-(void)labelandbutton{
    UILabel*blueLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 460, 70)];
    [blueLabel setText:@"                     EDIT PROFILE"];
    
    [blueLabel setTextColor:[UIColor whiteColor]];
    [blueLabel setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:0.8]];
    
    UIButton*backButton=[[UIButton alloc]initWithFrame:CGRectMake(0, 20, 50, 50)];
    
    
    [backButton setBackgroundImage:[UIImage imageNamed:@"back_button.png"] forState:UIControlStateNormal];
    
    
    [self.view addSubview:backButton];
    [self.view addSubview:blueLabel];
}

-(void)image{
    UIImageView*faceView=[[UIImageView alloc]initWithFrame:CGRectMake(94, 85, 113, 113)];
    [faceView setImage:[UIImage imageNamed:@"example.png"]];
    [self.view addSubview:faceView];
    }

-(void)infolable{
    UILabel *personelinfo=[[UILabel alloc]initWithFrame:CGRectMake(10, 200, 300, 40)];
    personelinfo.textColor=[UIColor blackColor];
    personelinfo.backgroundColor=[UIColor clearColor];
    personelinfo.textColor=[UIColor blackColor];
    personelinfo.userInteractionEnabled=NO;
    personelinfo.text=@"PERSONAL INFO";
    [self.view addSubview:personelinfo];

    }

-(void)lineView{
    UIView *lineView=[[UIView alloc]initWithFrame:CGRectMake(160, 220, 160, 2)];
    lineView.backgroundColor=[UIColor blackColor];
    [self.view addSubview:lineView];
}

-(void)lable{
    UILabel *lable=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 460, 70)];
    [lable setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:(0.8)]];
    [self.view addSubview:lable];
    
}
-(void)firstname{
    UITextField *firstname=[[UITextField alloc]initWithFrame:CGRectMake(18, 246, 294, 40)];
    firstname.placeholder=@"  steve";
    firstname.layer.borderWidth=1.0f;
    firstname.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:firstname];
    
}

-(void)lastname{
    UITextField *lastname=[[UITextField alloc]initWithFrame:CGRectMake(18, 296, 294, 40)];
    lastname.placeholder=@"  steve";
    lastname.layer.borderWidth=1.0f;
    lastname.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:lastname];
    
}

-(void)email{
    UITextField *emailfield=[[UITextField alloc]initWithFrame:CGRectMake(18, 346, 294, 40)];
    emailfield.placeholder=@"STEVE.STEPHEN@YAHOOMAIL.COM";
    emailfield.layer.borderWidth=1.0f;
    emailfield.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:emailfield];
    
}

-(void)phone{
    UITextField *phone=[[UITextField alloc]initWithFrame:CGRectMake(18, 396, 294, 40)];
    phone.placeholder=@"+1-444-242-6565";
    phone.layer.borderWidth=1.0f;
    phone.layer.borderColor=[[UIColor grayColor]CGColor];
    [self.view addSubview:phone];
    
}



-(void)buttons{
    
    UIButton*doneButton=[[UIButton alloc]initWithFrame:CGRectMake(18, 460, 294, 40)];
    [doneButton setTitle:@"DONE" forState:UIControlStateNormal];
    [doneButton setBackgroundColor:[UIColor colorWithRed:(0/255.0) green:(65/255.0) blue:(130/255.0) alpha:0.8]];
    
    [self.view addSubview:doneButton];
    
    
    UIButton*changeButton=[[UIButton alloc]initWithFrame:CGRectMake(18, 500, 294, 40)];
    [changeButton setTitle:@"       CHANGE PASSWORD" forState:UIControlStateNormal];
    
    [changeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.view addSubview:changeButton];
    
    }
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
